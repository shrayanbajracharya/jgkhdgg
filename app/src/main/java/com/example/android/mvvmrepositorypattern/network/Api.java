package com.example.android.mvvmrepositorypattern.network;

import com.example.android.mvvmrepositorypattern.data.remote.UserResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    @GET("users")
    Call<List<UserResponse>> getUsers();
}
