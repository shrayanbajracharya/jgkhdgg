package com.example.android.mvvmrepositorypattern.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.mvvmrepositorypattern.R;
import com.example.android.mvvmrepositorypattern.data.local.UserEntity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private List<UserEntity> userList;

    UserAdapter() {
        userList = new ArrayList<>();
    }

    public UserAdapter(ArrayList<UserEntity> userList) {
        this.userList = userList;
    }

    void setUserList(ArrayList<UserEntity> data) {
        userList.clear();
        userList.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View inflatedView = layoutInflater.inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        UserEntity currentUser = userList.get(position);

        holder.textViewUserId.setText("ID: " + currentUser.getUserId());
        holder.textViewUsername.setText("Username: " + currentUser.getUsername());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        TextView textViewUserId;
        TextView textViewUsername;

        UserViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewUserId = itemView.findViewById(R.id.user_id);
            textViewUsername = itemView.findViewById(R.id.username);
        }
    }
}
