package com.example.android.mvvmrepositorypattern.ui;

import android.content.Context;
import android.os.AsyncTask;

import com.example.android.mvvmrepositorypattern.app.AppDatabase;
import com.example.android.mvvmrepositorypattern.data.local.UserDao;
import com.example.android.mvvmrepositorypattern.data.local.UserEntity;

import java.util.List;

import androidx.lifecycle.MutableLiveData;

public class UserRepository {

    private MutableLiveData<List<UserEntity>> _users;
    private UserDao userDao;

    public UserRepository(Context context) {
        
        AppDatabase database = AppDatabase.getDatabaseInstance(context);
        userDao = database.userDao();
        
        _users = new MutableLiveData<>();
        
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                _users.postValue(userDao.getAllUsers());
            }
        });
    }

    public MutableLiveData<List<UserEntity>> getUsers() {
        return _users;
    }

    public void insertUser(UserEntity user) {
        new insertUserAsyncTask(userDao).execute(user);
    }

    public static class insertUserAsyncTask extends AsyncTask<UserEntity, Void, Void> {

        private UserDao userDao;

        insertUserAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(UserEntity... userEntities) {
            userDao.insertUser(userEntities[0]);
            return null;
        }
    }

    public void insertUsers(List<UserEntity> data) {
        new insertAllUsersAsyncTask(userDao).execute(data);
    }

    public static class insertAllUsersAsyncTask extends AsyncTask<List<UserEntity>, Void, Void> {

        private UserDao userDao;

        insertAllUsersAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(List<UserEntity>... lists) {
            userDao.insertUsers(lists[0]);
            return null;
        }
    }

    public void deleteAllUsers() {
        // new deleteAllUsersAsyncTask(userDao).execute();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                userDao.deleteAllUsers();
            }
        });
    }

    // Using AsyncTask instance to delete all notes (through repository)
    public static class deleteAllUsersAsyncTask extends AsyncTask<Void, Void, Void> {

        private UserDao userDao;

        private deleteAllUsersAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userDao.deleteAllUsers();
            return null;
        }
    }
}