package com.example.android.mvvmrepositorypattern.data.local;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_table")
public class UserEntity {

    @PrimaryKey
    private int userId;
    private String username;

    UserEntity() {
    }

    public UserEntity(int userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    public static UserEntity dummy() {
        UserEntity ue = new UserEntity();
        ue.username = "ashdgjashdkj";
        ue.userId = 47;
        return ue;
    }

    public int getUserId() {
        return userId;
    }

    void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    void setUsername(String username) {
        this.username = username;
    }
}
